package com.example.bilalanjum.loocdesignscreens8;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<Explore_Model> explorelist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView rv = (RecyclerView) findViewById(R.id.recycler_view);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rv.setLayoutManager(llm);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rv.getContext(),
                llm.getOrientation());
        rv.addItemDecoration(dividerItemDecoration);
        explorelist = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            Explore_Model info = new Explore_Model( R.drawable.btm_img, "Bam Rose Cinema", "Restaurant", "800 ft Fort Greece, 867 South", "Vernon Chang", "1 Week ago", "The place is friendly and the food is delicious. I love the atmosphere their drinks are delicious Me and the family try to come at least twice a month. The food is a little salty but it is worth and the price is reasonable, they have the best pizza so far.", 6);

            explorelist.add(info);
        }
        ExploreScreenAdapter ia = new ExploreScreenAdapter(explorelist);
        rv.setAdapter(ia);
    }
}
