package com.example.bilalanjum.loocdesignscreens8;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Bilal Anjum on 8/10/2017.
 */

public class ExploreScreenAdapter extends RecyclerView.Adapter<ExploreScreenAdapter.MyViewHolder> {


    private List<Explore_Model> explorelist;

    /**
     * View holder class
     */
    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView images;
        public TextView titleText;
        public TextView typeText;
        public TextView distanceText;
        public TextView title_profileText;
        public TextView feedback_givenText;
        public TextView feedback;
        public TextView reviewText;


        public MyViewHolder(View view) {
            super(view);

            images = (ImageView) view.findViewById(R.id.circle_image);
            titleText = (TextView) view.findViewById(R.id.title);
            typeText = (TextView) view.findViewById(R.id.type);
            distanceText = (TextView) view.findViewById(R.id.distance);
            title_profileText = (TextView) view.findViewById(R.id.title_profile);
            feedback_givenText = (TextView) view.findViewById(R.id.feedbackgiven);
            feedback = (TextView) view.findViewById(R.id.feedback);
            reviewText = (TextView) view.findViewById(R.id.reviewtag);
        }
    }

    public ExploreScreenAdapter(List<Explore_Model> explorelist) {
        this.explorelist = explorelist;
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Explore_Model c = explorelist.get(position);

        holder.titleText.setText(c.getTitle());
        holder.typeText.setText(c.getType());
        holder.distanceText.setText(c.getDistance());

        holder.title_profileText.setText(c.getTitle_profile());
        holder.feedback_givenText.setText(c.getFeedback_given());
        holder.feedback.setText(c.getFeedback());
        holder.reviewText.setText(c.getReview() + "");

    }

    @Override
    public int getItemCount() {
        return explorelist.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.explore_row, parent, false);
        return new MyViewHolder(v);
    }
}


