package com.example.bilalanjum.loocdesignscreens8;
/**
 * Created by Bilal Anjum on 8/10/2017.
 */
public class Explore_Model {

    protected int image;
    protected String title;
    protected String type;
    protected String distance;
    protected String title_profile;
    protected  String feedback_given;
    protected String feedback;
    protected  int review;

    public Explore_Model(int image, String title, String type, String distance, String title_profile,
                         String feedback_given, String feedback, int review) {
        this.image = image;
        this.title = title;
        this.type = type;
        this.distance = distance;
        this.title_profile = title_profile;
        this.feedback_given = feedback_given;
        this.feedback = feedback;
        this.review = review;

    }

    public String getTitle() {
        return title;
    }

    public int getImage() {
        return image;
    }

    public String getType() {
        return type;
    }

    public String getDistance() {
        return distance;
    }

    public int getReview() {
        return review;
    }

    public String getTitle_profile() {
        return title_profile;
    }

    public String getFeedback_given() {
        return feedback_given;
    }

    public String getFeedback() {
        return feedback;
    }
}


